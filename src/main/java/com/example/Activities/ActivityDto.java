package com.example.Activities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActivityDto {

    private String activity;
    private double accessibility;
    private String type;
    private int participants;
    private BigDecimal price;
    private String link;
    private String key;
}
