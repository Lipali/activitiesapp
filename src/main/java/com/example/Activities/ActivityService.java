package com.example.Activities;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@RequiredArgsConstructor
public class ActivityService {

    private RestTemplate restTemplate = new RestTemplate();
    private final ActivityRepository activityRepository;

    public ActivityDto getActivity() throws MyException {
        ActivityDto activityDto = restTemplate.getForObject("https://www.boredapi.com/api/activity/",ActivityDto.class);
        Activity activity = newActivity(activityDto);
        if (activityRepository.findActivityByActivityName(activity.getActivityName()).isEmpty()) {
            activityRepository.save(activity);
            return activityDto;
        }
        else
            throw new MyException("Już było");
    }
    public Activity newActivity (ActivityDto activityDto){
        Activity activity = Activity.builder()
                .activityName(activityDto.getActivity())
                .build();
        return activity;
    }

}
